//http://stm32.kosyak.info/doc/group___r_c_c___exported___functions.html

#ifndef BLUEPILL_H_
#define BLUEPILL_H_

#include <stddef.h>
#include "stm32f10x.h"
#include "stm32f10x_iwdg.h"

#define FALSE				0
#define TRUE				!FALSE
//#define LOW					0
//#define HIGH				!LOW



enum {
    PA0, PA1, PA2, PA3, PA4, PA5, PA6, PA7, PA8, PA9, PA10, PA11, PA12, PA13, PA14, PA15,
	PB0, PB1, PB2, PB3, PB4, PB5, PB6, PB7, PB8, PB9, PB10, PB11, PB12, PB13, PB14, PB15,
	PC0, PC1, PC2, PC3, PC4, PC5, PC6, PC7, PC8, PC9, PC10, PC11, PC12, PC13, PC14, PC15
};



uint16_t 		index_of(const char *, const char *, uint16_t);
GPIO_TypeDef * 	get_pin_port(uint8_t);
uint16_t 		get_pin_number(uint8_t);
uint8_t			check_parity(uint16_t integer);
uint32_t		random(uint32_t range);
void			adc_random_init(void);
uint32_t		adc_random(uint32_t range = 10);
void			wdog_init(uint8_t prescaler = IWDG_Prescaler_4);
void			wdog_reload(void);
void 			my_dma(size_t source_addr_param, size_t destination_addr_param, uint8_t number_of_bytes_param);
uint32_t		consecutive_numbers_sum(uint16_t number);
//int32_t			abs(int32_t value);

#endif /* BLUEPILL_H_ */
