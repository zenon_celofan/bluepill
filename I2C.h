#ifndef I2C_H_
#define I2C_H_

#include <stm32f10x_i2c.h>
#include "bluepill.h"
#include "Pin.h"

#define I2C_DIRECTION_TX 0
#define I2C_DIRECTION_RX 1
#define I2C1_DMA_CHANNEL_TX           DMA1_Channel6
#define I2C1_DMA_CHANNEL_RX           DMA1_Channel7
#define I2C2_DMA_CHANNEL_TX           DMA1_Channel4
#define I2C2_DMA_CHANNEL_RX           DMA1_Channel5
#define I2C1_DR_Address              0x40005410
#define I2C2_DR_Address              0x40005810

typedef enum {
    Error = 0,
    Success = !Error
} Status;


class I2C {
	class Pin sda_pin;
	class Pin scl_pin;
	I2C_TypeDef* i2c_x;
	DMA_InitTypeDef  I2CDMA_InitStructure;

	void init_i2c();
	void init_dma();
//	void reset_slave();
	void i2c_dma_config(const uint8_t * _buffer_p, uint32_t _buffer_size, uint32_t _direction);

public:
	I2C(I2C_TypeDef* _i2c);
	Status master_read(uint8_t* _buffer_p, uint32_t _number_of_bytes, uint8_t _slave_address);
	Status master_write(const uint8_t* _buffer_p, uint32_t _number_of_bytes, uint8_t _slave_address);
	Status master_write(uint8_t _first_byte, const uint8_t* _buffer_p, uint32_t _number_of_bytes, uint8_t _slave_address);
	void reset_slave(void);

};

#endif /* I2C_H_ */
