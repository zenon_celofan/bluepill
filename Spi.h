#ifndef SPI_H_
#define SPI_H_

#include <stdio.h>

#include "bluepill.h"
#include "stm32f10x_spi.h"
#include "Pin.h"

class Spi {
	SPI_TypeDef* spi;
	Pin miso_pin;
	Pin mosi_pin;
	Pin sck_pin;


	void init_spi(SPI_InitTypeDef* SPI_InitStructure_param);
	void init_pins(bool remap_param);

public:
	Spi() {};
	Spi(SPI_TypeDef* spi_x_param, SPI_InitTypeDef* SPI_InitStructure_param = NULL) : Spi(spi_x_param, false, SPI_InitStructure_param) {};
	Spi(SPI_TypeDef* spi_x_param, bool remap_param, SPI_InitTypeDef* SPI_InitStructure_param = NULL);

	void init_interrupt(void);
	void write_byte(u8 byte);
	u8 read_byte(u8 byte = 0x00);
	bool is_busy(void);

};

#endif /* SPI_H_ */
