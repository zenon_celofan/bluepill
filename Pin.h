#ifndef PIN_H_
#define PIN_H_

#include "stm32f10x.h"
#include "stm32f10x_gpio.h"

class Pin {

	GPIO_TypeDef* get_pin_port(uint8_t pin);
	uint16_t get_pin_number(uint8_t pin);

	void init_pin(void);
	void init_receive_interrupt(void);

public:

	GPIO_TypeDef*	port;
	u16				number;

	GPIOMode_TypeDef 	mode;
	GPIOSpeed_TypeDef 	speed;
	BitAction 			initial_state;


	Pin() = default;
	Pin(uint16_t gpio_pin, GPIOMode_TypeDef gpio_pin_mode = GPIO_Mode_Out_PP, GPIOSpeed_TypeDef gpio_pin_speed = GPIO_Speed_2MHz, BitAction	gpio_pin_state = Bit_RESET);
	Pin(uint16_t gpio_pin, BitAction gpio_pin_state) : Pin(gpio_pin, GPIO_Mode_Out_PP, GPIO_Speed_2MHz, gpio_pin_state) {};
	Pin(uint16_t gpio_pin, GPIOMode_TypeDef gpio_pin_mode, FunctionalState exti_interrupt);
	void digital_write(BitAction _new_state);
	void digital_write(uint8_t _new_state);
	BitAction digital_read(void);
	void digital_toggle(void);
};


#endif /* PIN_H_ */
