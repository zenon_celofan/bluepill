#ifndef SERIAL_H_
#define SERIAL_H_

#include "bluepill.h"
#include "stm32f10x.h"
#include "Pin.h"


#define TX_BUFFER_SIZE	1000
#define RX_BUFFER_SIZE	1000

class Serial {

	USART_TypeDef* usart_x;
	class Pin tx_pin;
	class Pin rx_pin;

	void init_usart(void);
	void init_receive_interrupt(void);
	void empty_callback(void) {};

public:
	uint16_t tx_characters_count;
	uint16_t rx_characters_count;
	char tx_buf[TX_BUFFER_SIZE];
	char rx_buf[RX_BUFFER_SIZE];
	uint16_t txp;
	uint16_t rxp;

	bool is_tx_busy;
	bool is_rx_busy;


	void (*receive_callback)(void) = NULL;


	Serial(USART_TypeDef* usart_number);
	void send_char(char);
	void send(const char*);
	void send(char*);
	void send_it(const char*);
	void receive_it_with_callback(void (*)(void));
};

#endif /* SERIAL_H_ */


//template <class Serial_t>
//void USARTx_IRQHandler(void) {
//	if (Serial_t::tx_characters_count != 0) {
//		if ((USART2->SR & USART_FLAG_TXE) != 0) {
//    		USART2->DR = (Serial_t::tx_buf[Serial_t::txp] & (uint16_t)0x01FF);
//    		Serial_t::txp++;
//    		Serial_t::tx_characters_count--;
//    		if (Serial_t::tx_characters_count == 0) {
//    			Serial_t::txp = 0;
//				USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
//				Serial_t::is_tx_busy = false;
//    		}
//    	}
//	}
//
//	//RX
//	if ((USART2->SR & USART_FLAG_RXNE) != 0) {
//		USART_ITConfig(USART2, USART_IT_IDLE, ENABLE);
//		Serial_t::rx_buf[Serial_t::rxp++] = (char) USART2->DR;
//		Serial_t::rx_characters_count++;
//	}
//	if ((USART2->SR & USART_FLAG_IDLE) != 0) {
//		USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
//		USART_ITConfig(USART2, USART_IT_IDLE, DISABLE);
//		Serial_t::rxp = 0;
//		Serial_t::receive_callback();
//		Serial_t::is_rx_busy = false;
//	}
//} //USARTx_IRQHandler()
