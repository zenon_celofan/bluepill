#ifndef RTC_H_
#define RTC_H_

#include "stm32f10x.h"


class Rtc {

public:

	int8_t time_zone;

	Rtc(void);
	uint8_t get_hours(void);
	uint8_t get_minutes(void);
	uint8_t get_seconds(void);
	void set_minutes(uint8_t _minutes, bool _reset_seconds = false);
	void set_hours(uint8_t _hours, bool _reset_seconds = false);
	void minutes_increment(void);
	void minutes_decrement(void);
	void hours_increment(void);
	void hours_decrement(void);
	void set_time(uint32_t counter);
	void one_second_interrupt(FunctionalState _enable = ENABLE);

private:

	GPIO_TypeDef * 	UTCp1_pin_port = GPIOB;
	uint16_t		UTCp1_pin_number = GPIO_Pin_0;

	GPIO_TypeDef * 	UTCp2_pin_port = GPIOB;
	uint16_t		UTCp2_pin_number = GPIO_Pin_1;

	void init_pins(void);
	void init_rtc(void);

};

#endif /* RTC_H_ */
