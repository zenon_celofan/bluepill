#include "IRQHandler.h"
#include "stm32f10x.h"
#include "stm32f10x_exti.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_usart.h"
#include "bluepill.h"
#include <stdio.h>
#include <stdlib.h>
#include "millis.h"
#include "micros.h"
#include "Serial.h"



extern Serial * usart1_serial;
extern Serial * usart2_serial;
extern Serial * usart3_serial;


extern volatile uint32_t milliseconds_after_reset;
extern volatile uint32_t microseconds_after_reset;


//micros() counter
void TIM3_IRQHandler(void) {

	TIM_ClearITPendingBit(TIM3, TIM_IT_Update);

	microseconds_after_reset++;

} //TIM3_IRQHandler()


//millis() counter
void TIM4_IRQHandler(void) {

	TIM_ClearITPendingBit(TIM4, TIM_IT_Update);

	milliseconds_after_reset++;

} //TIM4_IRQHandler()


void USART1_IRQHandler(void) {
	if (usart1_serial->tx_characters_count != 0) {
		if ((USART1->SR & USART_FLAG_TXE) != 0) {
    		USART1->DR = (usart1_serial->tx_buf[usart1_serial->txp] & (uint16_t)0x01FF);
    		usart1_serial->txp++;
    		usart1_serial->tx_characters_count--;
    		if (usart1_serial->tx_characters_count == 0) {
    			usart1_serial->txp = 0;
				USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
				usart1_serial->is_tx_busy = false;
    		}
    	}
	}

	//RX
	if ((USART1->SR & USART_FLAG_RXNE) != 0) {
		USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);
		usart1_serial->rx_buf[usart1_serial->rxp++] = (char) USART1->DR;
		usart1_serial->rx_characters_count++;
	}
	if ((USART1->SR & USART_FLAG_IDLE) != 0) {
		USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
		USART_ITConfig(USART1, USART_IT_IDLE, DISABLE);
		usart1_serial->rxp = 0;
		if (usart1_serial->receive_callback != NULL) {
			usart1_serial->receive_callback();
		}
		usart1_serial->is_rx_busy = false;
	}

} //USART1_IRQHandler()


void USART2_IRQHandler(void) {
	if (usart2_serial->tx_characters_count != 0) {
		if ((USART2->SR & USART_FLAG_TXE) != 0) {
    		USART2->DR = (usart2_serial->tx_buf[usart2_serial->txp] & (uint16_t)0x01FF);
    		usart2_serial->txp++;
    		usart2_serial->tx_characters_count--;
    		if (usart2_serial->tx_characters_count == 0) {
    			usart2_serial->txp = 0;
				USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
				usart2_serial->is_tx_busy = false;
    		}
    	}
	}

	//RX
	if ((USART2->SR & USART_FLAG_RXNE) != 0) {
		USART_ITConfig(USART2, USART_IT_IDLE, ENABLE);
		usart2_serial->rx_buf[usart2_serial->rxp++] = (char) USART2->DR;
		usart2_serial->rx_characters_count++;
	}
	if ((USART2->SR & USART_FLAG_IDLE) != 0) {
		USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
		USART_ITConfig(USART2, USART_IT_IDLE, DISABLE);
		usart2_serial->rxp = 0;
//		if (usart2_serial->receive_callback != NULL) {
//			usart2_serial->receive_callback();
//		}
		usart2_serial->is_rx_busy = false;
	}

} //USART2_IRQHandler()


void USART3_IRQHandler(void) {
	if (usart3_serial->tx_characters_count != 0) {
		if ((USART3->SR & USART_FLAG_TXE) != 0) {
    		USART3->DR = (usart3_serial->tx_buf[usart3_serial->txp] & (uint16_t)0x01FF);
    		usart3_serial->txp++;
    		usart3_serial->tx_characters_count--;
    		if (usart3_serial->tx_characters_count == 0) {
    			usart3_serial->txp = 0;
				USART_ITConfig(USART3, USART_IT_TXE, DISABLE);
				usart3_serial->is_tx_busy = false;
    		}
    	}
	}

	//RX
	if ((USART3->SR & USART_FLAG_RXNE) != 0) {
		USART_ITConfig(USART3, USART_IT_IDLE, ENABLE);
		usart3_serial->rx_buf[usart3_serial->rxp++] = (char) USART3->DR;
		usart3_serial->rx_characters_count++;
	}
	if ((USART3->SR & USART_FLAG_IDLE) != 0) {
		USART_ITConfig(USART3, USART_IT_RXNE, DISABLE);
		USART_ITConfig(USART3, USART_IT_IDLE, DISABLE);
		usart3_serial->rxp = 0;
//		if (usart3_serial->receive_callback != NULL) {
//			usart3_serial->receive_callback();
//		}
		usart3_serial->is_rx_busy = false;
	}

} //USART3_IRQHandler()
