#ifndef MICROS_H_
#define MICROS_H_

#include "stm32f10x.h"



void 		micros_init(void);
uint32_t 	micros(void);
void		delay_us(uint32_t microseconds);



#endif /* MICROS_H_ */
