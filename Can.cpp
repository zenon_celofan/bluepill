#include "Can.h"
//#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_can.h"

Can::Can(CAN_TypeDef* can_number) : Can(can_number, false) {
} //Can()



Can::Can(CAN_TypeDef* can_number, bool remap) {

	can_x = can_number;

	if (can_x == CAN1) {
		if (remap == true) {
			GPIO_PinRemapConfig(GPIO_Remap1_CAN1,ENABLE);
			can_tx_pin  = Pin(PB9, GPIO_Mode_AF_PP, GPIO_Speed_50MHz);
			can_rx_pin  = Pin(PB8, GPIO_Mode_IPU);
		} else {
			can_tx_pin  = Pin(PA12, GPIO_Mode_AF_PP, GPIO_Speed_50MHz);
			can_rx_pin  = Pin(PA11, GPIO_Mode_IPU);
		}
	}

	init_can();
} //Can()



void Can::init_can(void) {

	if (can_x == CAN1) {
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
	}

    CAN_DeInit(CAN1);
    CAN_InitTypeDef	CAN_InitStructure;
    CAN_StructInit(&CAN_InitStructure);

    CAN_InitStructure.CAN_TTCM = DISABLE;
    CAN_InitStructure.CAN_ABOM = DISABLE;
    CAN_InitStructure.CAN_AWUM = DISABLE;
    CAN_InitStructure.CAN_NART = ENABLE;
    CAN_InitStructure.CAN_RFLM = DISABLE;
    CAN_InitStructure.CAN_TXFP = DISABLE;
    CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
    //CAN_InitStructure.CAN_Mode = CAN_Mode_LoopBack;
    CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;  //4,1
    CAN_InitStructure.CAN_BS1 = CAN_BS1_12tq;  //16,6
    CAN_InitStructure.CAN_BS2 = CAN_BS2_5tq;  //8,2
    CAN_InitStructure.CAN_Prescaler = 8;	//16

    CAN_Init(can_x, &CAN_InitStructure);

	//GPIO_PinRemapConfig(GPIO_Remap1_CAN1 , ENABLE);

    /* CAN filter init */
    CAN_FilterInitTypeDef  CAN_FilterInitStructure;

    CAN_FilterInitStructure.CAN_FilterNumber=0;
    CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask;
    CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit;
    CAN_FilterInitStructure.CAN_FilterIdHigh=0x0000;
    CAN_FilterInitStructure.CAN_FilterIdLow=0x0000;
    CAN_FilterInitStructure.CAN_FilterMaskIdHigh=0x0000;
    CAN_FilterInitStructure.CAN_FilterMaskIdLow=0x0000;
    CAN_FilterInitStructure.CAN_FilterFIFOAssignment=CAN_FIFO0;
    CAN_FilterInitStructure.CAN_FilterActivation=ENABLE;		//Must be ENABLE'd to receive anything
    CAN_FilterInit(&CAN_FilterInitStructure);

} //init_can()


void Can::init_interrupt(void) {

	NVIC_InitTypeDef   NVIC_InitStructure;

    CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);

    NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn; //CAN1_RX0_IRQn; USB_LP_CAN1_RX0_IRQn
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0xFF; //0xFF;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0xFF; //0xFF;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

} //init_interrupt()



bool Can::is_message_pending(void) {

	if (CAN_MessagePending(can_x, CAN_FIFO0) != 0) {
		return true;
	} else {
		return false;
	}

} //is_message_pending()



void Can::receive_message(CanRxMsg * pRxMessage) {

	pRxMessage->StdId = 0x00;
	pRxMessage->IDE = CAN_ID_STD;
	pRxMessage->DLC = 0;
	pRxMessage->Data[0] = 0x00;
	pRxMessage->Data[1] = 0x00;
	CAN_Receive(can_x, CAN_FIFO0, pRxMessage);

} //receive_message()



void Can::send_message(CanTxMsg * pTxMessage) {

	CAN_Transmit(can_x, pTxMessage);

} //send_message()
