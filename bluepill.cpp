#include "bluepill.h"
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_adc.h"
#include "stm32f10x_crc.h"
#include "stm32f10x_tim.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



uint16_t index_of(const char * str1, const char * str2, uint16_t starting_from_index ) {

	uint16_t len = strlen(str1);
	uint16_t return_value;



	if (starting_from_index < len) {

		char * found = strstr(str1 + starting_from_index, str2);

		if (found != NULL) {
			return_value = found - str1;
		} else return_value = len;

	} else return_value = len;

	return return_value;

} //index_of()


GPIO_TypeDef* get_pin_port(uint8_t pin) {

	GPIO_TypeDef * pin_port;

	pin = pin >> 4;

	if (pin == 0) {
		pin_port = GPIOA;
	} else if (pin == 1) {
		pin_port = GPIOB;
	} else {
		pin_port = GPIOC;
	}

	return pin_port;

} //pin_port()


uint16_t get_pin_number(uint8_t pin) {

	uint16_t pin_number = 0;
	const uint8_t PIN_NUMBER_MASK = 0b00001111;

	pin = pin & PIN_NUMBER_MASK;

	if (pin == 0) {
		pin_number = GPIO_Pin_0;
	} else 	if (pin == 1) {
		pin_number = GPIO_Pin_1;
	} else 	if (pin == 2) {
		pin_number = GPIO_Pin_2;
	} else 	if (pin == 3) {
		pin_number = GPIO_Pin_3;
	} else 	if (pin == 4) {
		pin_number = GPIO_Pin_4;
	} else 	if (pin == 5) {
		pin_number = GPIO_Pin_5;
	} else 	if (pin == 6) {
		pin_number = GPIO_Pin_6;
	} else 	if (pin == 7) {
		pin_number = GPIO_Pin_7;
	} else 	if (pin == 8) {
		pin_number = GPIO_Pin_8;
	} else 	if (pin == 9) {
		pin_number = GPIO_Pin_9;
	} else 	if (pin == 10) {
		pin_number = GPIO_Pin_10;
	} else 	if (pin == 11) {
		pin_number = GPIO_Pin_11;
	} else 	if (pin == 12) {
		pin_number = GPIO_Pin_12;
	} else 	if (pin == 13) {
		pin_number = GPIO_Pin_13;
	} else 	if (pin == 14) {
		pin_number = GPIO_Pin_14;
	} else 	if (pin == 15) {
		pin_number = GPIO_Pin_15;
	}

	return pin_number;

} //pin_port()



uint8_t	check_parity(uint16_t integer) {

	uint8_t parity = 0;

	while (integer != 0) {
		parity += integer & 0x1;
		integer = integer >> 1;
	}

	parity %= 2;

	return parity;
}  //check_parity()



uint32_t random(uint32_t range) {
	return random() % range;
} //random()



void adc_random_init(void) {
	ADC_InitTypeDef ADC_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	RCC_ADCCLKConfig(RCC_PCLK2_Div2);

	ADC_DeInit(ADC1);
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	ADC_Init(ADC1, &ADC_InitStructure);

	//enable internal temperature channel
	ADC_TempSensorVrefintCmd(ENABLE);

	// Enable ADCperipheral
	ADC_Cmd(ADC1, ENABLE);

	//Convert the ADC1 temperature sensor, user shortest sample time to generate most noise
	ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 1, ADC_SampleTime_1Cycles5);

	// Enable CRC clock
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE);

	//disable ADC1 to save power
	ADC_Cmd(ADC1, DISABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, DISABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, DISABLE);
} //adc_random_init



uint32_t adc_random(uint32_t range) {
	uint32_t random_number;

	// Enable ADCperipheral
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE);
	ADC_Cmd(ADC1, ENABLE);

	for (uint8_t i = 0; i < 8; i++) {
		//Start ADC1 Software Conversion
		ADC_SoftwareStartConvCmd(ADC1, ENABLE);
		//wait for conversion complete
		while (!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC));

		random_number = CRC_CalcCRC(ADC_GetConversionValue(ADC1));
		//clear EOC flag
		ADC_ClearFlag(ADC1, ADC_FLAG_EOC);
	}

	//disable ADC1 to save power
	ADC_Cmd(ADC1, DISABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, DISABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, DISABLE);

	return random_number % range;
} //adc_random()



void wdog_init(uint8_t prescaler) {

	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	IWDG_SetPrescaler(prescaler);
	IWDG_SetReload(0xFFF);
	IWDG_ReloadCounter();
	IWDG_Enable();                                  // IWDG + LSI enable

} //wdog_init()



void wdog_reload(void) {

	IWDG_ReloadCounter();

} //wdog_reload()



void my_dma(size_t source_addr_param, size_t destination_addr_param, uint8_t number_of_bytes_param) {

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	DMA_DeInit(DMA1_Channel1);

	DMA_InitTypeDef DMA_InitStruct;
	DMA_StructInit(&DMA_InitStruct);


	DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t) source_addr_param;
	DMA_InitStruct.DMA_MemoryBaseAddr = (uint32_t) destination_addr_param;
	DMA_InitStruct.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStruct.DMA_BufferSize = number_of_bytes_param;
	DMA_InitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Enable;
	DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStruct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStruct.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStruct.DMA_Priority = DMA_Priority_High;
	DMA_InitStruct.DMA_M2M = DMA_M2M_Enable;

	DMA_Init(DMA1_Channel1, &DMA_InitStruct);

	DMA_Cmd(DMA1_Channel1, ENABLE);


} //my_dma()


uint32_t consecutive_numbers_sum(uint16_t number) {
	uint32_t sum = 0;
	for (uint16_t i = 1; i <= number; ++i) {
		sum += i;
	}
	return sum;
} //consecutive_numbers_sum()

//
//int32_t abs(int32_t value) {
//	if (value >= 0) {
//		return value;
//	} else {
//		return -value;
//	}
//} //abs()
