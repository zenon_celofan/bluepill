#ifndef MILLIS_H_
#define MILLIS_H_

#include "stm32f10x.h"



void 		millis_init(void);
uint32_t 	millis(void);
void 		delay(uint16_t milliseconds);



#endif /* MILLIS_H_ */
