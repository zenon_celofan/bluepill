#ifndef CAN_H_
#define CAN_H_

#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "Pin.h"
#include "bluepill.h"

class Can {

	class Pin can_tx_pin;
	class Pin can_rx_pin;
	CAN_TypeDef* can_x;

	void init_can(void);


public:
	Can(CAN_TypeDef* can_number);
	Can(CAN_TypeDef* can_number, bool remap);

	bool is_message_pending(void);
	void receive_message(CanRxMsg * pRxMessage);
	void send_message(CanTxMsg * pTxMessage);
	void init_interrupt(void);

};

#endif /* CAN_H_ */
