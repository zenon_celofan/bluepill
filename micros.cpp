#include "micros.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"



volatile uint32_t microseconds_after_reset = 0;



void micros_init(void) {

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

    TIM_TimeBaseInitTypeDef timerInitStructure;
    timerInitStructure.TIM_Prescaler = 1;
    timerInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    timerInitStructure.TIM_Period = 35; // will get irq each 1us on TIMx->CNT overflow (manually tuned value)
    timerInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInit(TIM3, &timerInitStructure);
    TIM_Cmd(TIM3, ENABLE);

    NVIC_InitTypeDef NVIC_InitStruct;
    NVIC_InitStruct.NVIC_IRQChannel = TIM3_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
    //NVIC_InitStruct.NVIC_IRQChannelSubPriority = 3;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    TIM_ClearITPendingBit(TIM3,TIM_IT_Update);
    NVIC_Init(&NVIC_InitStruct);
    TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
} //micros_init()



uint32_t micros(void) {

	return microseconds_after_reset;

} //micros()



void delay_us(uint32_t microseconds) {

	uint32_t d = micros();

	while (micros() - d <= microseconds);

} //delay_us()
