#include "millis.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"



volatile uint32_t milliseconds_after_reset = 0;



void millis_init(void) {

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

    TIM_TimeBaseInitTypeDef timerInitStructure;
    // if MCU frequency 72 MHz, prescaler of value 72 will make 1us counter steps
    timerInitStructure.TIM_Prescaler = 72 - 1;
    timerInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    timerInitStructure.TIM_Period = 1000 - 1; // will get irq each 1ms on TIMx->CNT overflow
    timerInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInit(TIM4, &timerInitStructure);
    TIM_Cmd(TIM4, ENABLE);

    NVIC_InitTypeDef NVIC_InitStruct;
    NVIC_InitStruct.NVIC_IRQChannel = TIM4_IRQn;
    /*for more accuracy irq priority should be higher, but now its default*/
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 2;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 4;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    TIM_ClearITPendingBit(TIM4,TIM_IT_Update);
    NVIC_Init(&NVIC_InitStruct);
    TIM_ITConfig(TIM4,TIM_IT_Update,ENABLE);
} //millis_init()



uint32_t millis(void) {

	return milliseconds_after_reset;

} //millis()



void delay(uint16_t milliseconds) {

	uint32_t d = millis();

	while (millis() - d <= milliseconds);

} //delay()
