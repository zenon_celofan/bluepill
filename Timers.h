#ifndef TIMERS_H_
#define TIMERS_H_

#include "bluepill.h"
#include "millis.h"

/* Example usage:
 *
 * 	Timers<4> timers;
 *
 *  timers.attach(0, 1000, led_blinker);
 *
 *  void led_blinker(void) {
 *  	led.toggle();
 *  }
 *
 */

struct timer_element {
  void (*user_function)(void);
  u32 interval;
  u32 start_time;
  u16 repeats;
};



template<u8 NUMBER_OF_TIMERS>
class Timers {

private:
    struct timer_element element[NUMBER_OF_TIMERS];

public:
    Timers(void) {
      for (u8 i = 0; i < NUMBER_OF_TIMERS; i++) {
        element[i].user_function = 0;
        element[i].interval = 0;
        element[i].start_time = 0;
        element[i].repeats = 0;
      }

    } //Timers()



    void attach(u8 element_number, u32 interval_param, void (*function_pointer)(void), u16 repeats_param = 0) {
      element[element_number].user_function = function_pointer;
      element[element_number].interval = interval_param;
      element[element_number].start_time = millis();
      element[element_number].repeats = repeats_param;
    } //attach()



    void set_interval(u8 element_number, u32 time_period) {
      element[element_number].interval = time_period;
      element[element_number].start_time = millis();
    } //set_interval()



    void update_interval(u8 element_number, u32 time_period) {
      element[element_number].interval = time_period;
    } //update_interval()



    void process(void) {
      u32 actual_time = millis();

      for (u8 i = 0; i < NUMBER_OF_TIMERS; i++) {
        u32 delta_time = actual_time - element[i].start_time;

        if ( (element[i].interval > 0) && (delta_time >= element[i].interval) ) {
            element[i].start_time += delta_time;
        	element[i].user_function();

        	if (element[i].repeats > 0) {
        		--element[i].repeats;
        		if (element[i].repeats == 0) {
        			element[i].interval = 0;
        		}
        	}
        }
      }
    } //process()
};

#endif /* TIMERS_H_ */
