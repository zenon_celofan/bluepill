#include "Serial.h"
#include "IRQHandler.h"
#include "stddef.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_rcc.h"

Serial * usart1_serial;
Serial * usart2_serial;
Serial * usart3_serial;


Serial::Serial(USART_TypeDef* usart_number) {
	usart_x = usart_number;

	if (usart_x == USART1) {
		tx_pin = Pin(PA9, GPIO_Mode_AF_PP);
		rx_pin = Pin(PA10, GPIO_Mode_IPU);
		usart1_serial = this;
	} else 	if (usart_x == USART2) {
		tx_pin = Pin(PA2, GPIO_Mode_AF_PP);
		rx_pin = Pin(PA3, GPIO_Mode_IPU);
		usart2_serial = this;
	} else 	if (usart_x == USART3) {
		tx_pin = Pin(PB10, GPIO_Mode_AF_PP);
		rx_pin = Pin(PB11, GPIO_Mode_IPU);
		usart3_serial = this;
	}

	is_rx_busy = false;
	is_tx_busy = false;
	init_usart();
	init_receive_interrupt();
} //Serial()



void Serial::init_usart(void) {
	if (usart_x == USART1) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	}
	else if (usart_x == USART2) {
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	}
	else if (usart_x == USART3) {
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	}

	USART_InitTypeDef USART_InitStructure;
	USART_StructInit(&USART_InitStructure);
	USART_InitStructure.USART_BaudRate = 9600;
	USART_Init(usart_x, &USART_InitStructure);
	USART_Cmd(usart_x, ENABLE);
} //init_usart()


void Serial::init_receive_interrupt(void) {

	NVIC_InitTypeDef NVIC_InitStructure;

	if (usart_x == USART1) {
		NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x07;
	}
	else if (usart_x == USART2) {
		NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x08;
	}
	else if (usart_x == USART3) {
		NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x09;
	}
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_ITConfig(usart_x, USART_IT_TXE, DISABLE);
	USART_ITConfig(usart_x, USART_IT_RXNE, DISABLE);
	USART_ITConfig(usart_x, USART_IT_IDLE, DISABLE);

} //init_receive_interrupt()


void Serial::send_char(char c) {
    while (USART_GetFlagStatus(usart_x, USART_FLAG_TXE) == RESET);
    USART_SendData(usart_x, c);
} //send_char()


void Serial::send_it(const char* s) {
	tx_characters_count = 0;
	txp = 0;
	while (*s != 0) {
		tx_buf[tx_characters_count++] = *s++;
	}
	is_tx_busy = true;
	USART_ITConfig(usart_x, USART_IT_TXE, ENABLE);
} //send_it()


void Serial::send(const char* s) {
    while (*s)
        send_char(*s++);
} //send()


void Serial::send(char* s) {
    while (*s)
        send_char(*s++);
} //send()


void Serial::receive_it_with_callback(void (*callback_function)(void)) {
	rx_characters_count = 0;
	receive_callback = callback_function;
	is_rx_busy = true;
	USART_ITConfig(usart_x, USART_IT_RXNE, ENABLE);
} //receive_it_with_callback()
