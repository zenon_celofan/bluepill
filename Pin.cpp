#include "Pin.h"
#include "bluepill.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"




Pin::Pin(uint16_t gpio_pin, GPIOMode_TypeDef gpio_pin_mode, GPIOSpeed_TypeDef gpio_pin_speed, BitAction	gpio_pin_state) {
	port = get_pin_port(gpio_pin);
	number = get_pin_number(gpio_pin);
	mode = gpio_pin_mode;
	speed = gpio_pin_speed;
	initial_state = gpio_pin_state;

	if (gpio_pin == PA15 || gpio_pin == PB3 || gpio_pin == PB4) {
		GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	}
	init_pin();
} //Pin()


Pin::Pin(uint16_t gpio_pin, GPIOMode_TypeDef gpio_pin_mode, FunctionalState exti_interrupt) {
	port = get_pin_port(gpio_pin);
	number = get_pin_number(gpio_pin);
	mode = gpio_pin_mode;
	speed = GPIO_Speed_2MHz;
	initial_state = Bit_RESET;

	if (gpio_pin == PA15 || gpio_pin == PB3 || gpio_pin == PB4) {
		 GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
	}
	init_pin();
	if (exti_interrupt == ENABLE) {
		init_receive_interrupt();
	}
} //Pin()

GPIO_TypeDef* Pin::get_pin_port(uint8_t pin) {

	GPIO_TypeDef * pin_port;

	pin = pin >> 4;

	if (pin == 0) {
		pin_port = GPIOA;
	} else if (pin == 1) {
		pin_port = GPIOB;
	} else {
		pin_port = GPIOC;
	}

	return pin_port;

} //pin_port()


uint16_t Pin::get_pin_number(uint8_t pin) {

	uint16_t pin_number = 0;
	const uint8_t PIN_NUMBER_MASK = 0b00001111;

	pin = pin & PIN_NUMBER_MASK;

	if (pin == 0) {
		pin_number = GPIO_Pin_0;
	} else 	if (pin == 1) {
		pin_number = GPIO_Pin_1;
	} else 	if (pin == 2) {
		pin_number = GPIO_Pin_2;
	} else 	if (pin == 3) {
		pin_number = GPIO_Pin_3;
	} else 	if (pin == 4) {
		pin_number = GPIO_Pin_4;
	} else 	if (pin == 5) {
		pin_number = GPIO_Pin_5;
	} else 	if (pin == 6) {
		pin_number = GPIO_Pin_6;
	} else 	if (pin == 7) {
		pin_number = GPIO_Pin_7;
	} else 	if (pin == 8) {
		pin_number = GPIO_Pin_8;
	} else 	if (pin == 9) {
		pin_number = GPIO_Pin_9;
	} else 	if (pin == 10) {
		pin_number = GPIO_Pin_10;
	} else 	if (pin == 11) {
		pin_number = GPIO_Pin_11;
	} else 	if (pin == 12) {
		pin_number = GPIO_Pin_12;
	} else 	if (pin == 13) {
		pin_number = GPIO_Pin_13;
	} else 	if (pin == 14) {
		pin_number = GPIO_Pin_14;
	} else 	if (pin == 15) {
		pin_number = GPIO_Pin_15;
	}

	return pin_number;

} //pin_port()



void Pin::init_pin() {

	if (port == GPIOA) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	} else if (port == GPIOB) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	} else if (port == GPIOC) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	}

    if ((mode == GPIO_Mode_Out_OD) || (mode == GPIO_Mode_Out_PP)) {
    	GPIO_WriteBit(port, number, initial_state);
    } else if ((mode == GPIO_Mode_AF_OD) || (mode == GPIO_Mode_AF_PP)) {
    	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    }

	GPIO_InitTypeDef  GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = number;
	GPIO_InitStructure.GPIO_Mode = mode;
	GPIO_InitStructure.GPIO_Speed = speed;
    GPIO_Init(port, &GPIO_InitStructure);
} //init_pin()



void Pin::init_receive_interrupt(void) {

	NVIC_InitTypeDef NVIC_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	unsigned char exti_port_source;
	unsigned char exti_pin_source;

	if (number == GPIO_Pin_0) {
		NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
	} else if (number == GPIO_Pin_1) {
		NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;
	} else if (number == GPIO_Pin_2) {
		NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;
	} else if (number == GPIO_Pin_3) {
		NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;
	} else if (number == GPIO_Pin_4) {
		NVIC_InitStructure.NVIC_IRQChannel = EXTI4_IRQn;
	} else if (  (number == GPIO_Pin_5)
			  || (number == GPIO_Pin_6)
			  || (number == GPIO_Pin_7)
			  || (number == GPIO_Pin_8)
			  || (number == GPIO_Pin_9) ) {
		NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
	} else {
		NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
	}
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 10;
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	if (number == GPIO_Pin_0) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line0;
		exti_pin_source = GPIO_PinSource0;
	} else if (number == GPIO_Pin_1) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line1;
		exti_pin_source = GPIO_PinSource1;
	} else if (number == GPIO_Pin_2) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line2;
		exti_pin_source = GPIO_PinSource2;
	} else if (number == GPIO_Pin_3) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line3;
		exti_pin_source = GPIO_PinSource3;
	} else if (number == GPIO_Pin_4) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line4;
		exti_pin_source = GPIO_PinSource4;
	} else if (number == GPIO_Pin_5) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line5;
		exti_pin_source = GPIO_PinSource5;
	} else if (number == GPIO_Pin_6) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line6;
		exti_pin_source = GPIO_PinSource6;
	} else if (number == GPIO_Pin_7) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line7;
		exti_pin_source = GPIO_PinSource7;
	} else if (number == GPIO_Pin_8) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line8;
		exti_pin_source = GPIO_PinSource8;
	} else if (number == GPIO_Pin_9) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line9;
		exti_pin_source = GPIO_PinSource9;
	} else if (number == GPIO_Pin_10) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line10;
		exti_pin_source = GPIO_PinSource10;
	} else if (number == GPIO_Pin_11) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line11;
		exti_pin_source = GPIO_PinSource11;
	} else if (number == GPIO_Pin_12) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line12;
		exti_pin_source = GPIO_PinSource12;
	} else if (number == GPIO_Pin_13) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line13;
		exti_pin_source = GPIO_PinSource13;
	} else if (number == GPIO_Pin_14) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line14;
		exti_pin_source = GPIO_PinSource14;
	} else if (number == GPIO_Pin_15) {
		EXTI_InitStructure.EXTI_Line = EXTI_Line15;
		exti_pin_source = GPIO_PinSource15;
	}
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;


	if (port == GPIOA) {
		exti_port_source = GPIO_PortSourceGPIOA;
	} else if (port == GPIOB) {
		exti_port_source = GPIO_PortSourceGPIOB;
	} else if (port == GPIOC) {
		exti_port_source = GPIO_PortSourceGPIOC;
	}

	GPIO_EXTILineConfig(exti_port_source, exti_pin_source);
	EXTI_Init(&EXTI_InitStructure);
} //init_receive_interrupt()



void Pin::digital_write(BitAction _new_state) {
	GPIO_WriteBit(port, number, _new_state);
} //digital_write()



void Pin::digital_write(uint8_t _new_state) {
	if (_new_state != 0) {
		GPIO_WriteBit(port, number, Bit_SET);
	} else {
		GPIO_WriteBit(port, number, Bit_RESET);
	}
} //digital_write()



BitAction Pin::digital_read(void) {
	return (BitAction) GPIO_ReadInputDataBit(port, number);
} //digital_read



void Pin::digital_toggle(void) {
	port->ODR ^= number;
} //digital_toggle
