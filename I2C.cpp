#include "I2C.h"
#include "Pin.h"
#include "stm32f10x_i2c.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_dma.h"


I2C::I2C(I2C_TypeDef* i2c_number) {
	i2c_x = i2c_number;

	reset_slave();
	if (i2c_x == I2C1) {
		sda_pin = Pin(PB7, GPIO_Mode_AF_OD, GPIO_Speed_50MHz);
		scl_pin = Pin(PB6, GPIO_Mode_AF_OD, GPIO_Speed_50MHz);
	} else if (i2c_x == I2C2) {
		sda_pin = Pin(PB11, GPIO_Mode_AF_OD, GPIO_Speed_50MHz);
		scl_pin = Pin(PB10, GPIO_Mode_AF_OD, GPIO_Speed_50MHz);
	}
	init_i2c();
	init_dma();
} //I2C()


void I2C::init_i2c(void) {
	I2C_InitTypeDef  I2C_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	if (i2c_x == I2C1)
	{
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C1, ENABLE);
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C1, DISABLE);
	} else if (i2c_x == I2C2) {
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE);
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C2, ENABLE);
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_I2C2, DISABLE);
	}

	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_OwnAddress1 = 0x00;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed = 400000;
	I2C_Init(I2C1, &I2C_InitStructure);
	I2C_InitStructure.I2C_OwnAddress1 = 0x00;
	I2C_Init(I2C2, &I2C_InitStructure);
} //init_i2c()


void I2C::init_dma(void) {

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	if (i2c_x == I2C1) {
		/* I2C1 TX DMA Channel configuration */
		DMA_DeInit(I2C1_DMA_CHANNEL_TX);
		I2CDMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) I2C1_DR_Address;
		I2CDMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) 0;  /* This parameter will be configured durig communication */
		I2CDMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;    /* This parameter will be configured durig communication */
		I2CDMA_InitStructure.DMA_BufferSize = 0xFFFF;            /* This parameter will be configured durig communication */
		I2CDMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
		I2CDMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
		I2CDMA_InitStructure.DMA_PeripheralDataSize = DMA_MemoryDataSize_Byte;
		I2CDMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
		I2CDMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
		I2CDMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
		I2CDMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
		DMA_Init(I2C1_DMA_CHANNEL_TX, &I2CDMA_InitStructure);
		/* I2C1 RX DMA Channel configuration */
		DMA_DeInit(I2C1_DMA_CHANNEL_RX);
		DMA_Init(I2C1_DMA_CHANNEL_RX, &I2CDMA_InitStructure);
	} else if (i2c_x == I2C2) {
		/* I2C2 TX DMA Channel configuration */
		DMA_DeInit(I2C2_DMA_CHANNEL_TX);
		I2CDMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) I2C2_DR_Address;
		I2CDMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) 0;  /* This parameter will be configured durig communication */
		I2CDMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;    /* This parameter will be configured durig communication */
		I2CDMA_InitStructure.DMA_BufferSize = 0xFFFF;            /* This parameter will be configured durig communication */
		I2CDMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
		I2CDMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
		I2CDMA_InitStructure.DMA_PeripheralDataSize = DMA_MemoryDataSize_Byte;
		I2CDMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
		I2CDMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
		I2CDMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
		I2CDMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
		DMA_Init(I2C2_DMA_CHANNEL_TX, &I2CDMA_InitStructure);
		/* I2C2 RX DMA Channel configuration */
		DMA_DeInit(I2C2_DMA_CHANNEL_RX);
		DMA_Init(I2C2_DMA_CHANNEL_RX, &I2CDMA_InitStructure);
	}
} //init_dma()


void I2C::reset_slave(void) {
	//if slave is forcing sda line low, toggling scl line few times will unlock slave device
	if (i2c_x == I2C1) {
		scl_pin = Pin(PB6, GPIO_Mode_AF_OD, GPIO_Speed_2MHz);
	} else 	if (i2c_x == I2C2) {
		scl_pin = Pin(PB10, GPIO_Mode_AF_OD, GPIO_Speed_2MHz);
	}

	for (uint8_t i = 0; i < 16; ++i) {
		scl_pin.digital_toggle();
		for (uint8_t j = 0; j < 100;){
			++j;
		}
	}
} //reset_i2c_slave()


void I2C::i2c_dma_config(const uint8_t* _buffer_p, uint32_t _buffer_size, uint32_t _direction) {

    /* Initialize the DMA with the new parameters */
    if (_direction == I2C_DIRECTION_TX) {
        I2CDMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) _buffer_p;
        I2CDMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
        I2CDMA_InitStructure.DMA_BufferSize = (uint32_t) _buffer_size;

        if (i2c_x == I2C1) {
            I2CDMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) I2C1_DR_Address;
            DMA_Cmd(I2C1_DMA_CHANNEL_TX, DISABLE);
            DMA_Init(I2C1_DMA_CHANNEL_TX, &I2CDMA_InitStructure);
            DMA_Cmd(I2C1_DMA_CHANNEL_TX, ENABLE);
        } else if (i2c_x == I2C2) {
            I2CDMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) I2C2_DR_Address;
            DMA_Cmd(I2C2_DMA_CHANNEL_TX, DISABLE);
            DMA_Init(I2C2_DMA_CHANNEL_TX, &I2CDMA_InitStructure);
            DMA_Cmd(I2C2_DMA_CHANNEL_TX, ENABLE);
        }
    } else if (_direction == I2C_DIRECTION_RX) {
        I2CDMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) _buffer_p;
        I2CDMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
        I2CDMA_InitStructure.DMA_BufferSize = (uint32_t) _buffer_size;
        if (i2c_x == I2C1) {
            I2CDMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) I2C1_DR_Address;
            DMA_Cmd(I2C1_DMA_CHANNEL_RX, DISABLE);
            DMA_Init(I2C1_DMA_CHANNEL_RX, &I2CDMA_InitStructure);
            DMA_Cmd(I2C1_DMA_CHANNEL_RX, ENABLE);
        } else if (i2c_x == I2C2) {
            I2CDMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) I2C2_DR_Address;
            DMA_Cmd(I2C2_DMA_CHANNEL_RX, DISABLE);
            DMA_Init(I2C2_DMA_CHANNEL_RX, &I2CDMA_InitStructure);
            DMA_Cmd(I2C2_DMA_CHANNEL_RX, ENABLE);
        }
    } //direction
} //i2c_dma_config()


Status I2C::master_read(uint8_t* _buffer_p, uint32_t _number_of_bytes, uint8_t _slave_address) {
    __IO uint32_t temp = 0;
    __IO uint32_t Timeout = 0;

    // Enable I2C errors interrupts
    //i2c_x->CR2 |= I2C_IT_ERR;

	/* Configure I2Cx DMA channel */
    i2c_dma_config(_buffer_p, _number_of_bytes, I2C_DIRECTION_RX);
	/* Set Last bit to have a NACK on the last received byte */
    i2c_x->CR2 |= CR2_LAST_Set;
	/* Enable I2C DMA requests */
    i2c_x->CR2 |= CR2_DMAEN_Set;
	Timeout = 0xFFFF;
	/* Send START condition */
	i2c_x->CR1 |= CR1_START_Set;
	/* Wait until SB flag is set: EV5  */
	while ((i2c_x->SR1 & 0x0001) != 0x0001) {
		if (Timeout-- == 0)
			return Error;
	}
	Timeout = 0xFFFF;
        /* Send slave address */
        /* Set the address bit0 for read */
	_slave_address |= OAR1_ADD0_Set;
	i2c_x->DR = _slave_address;
	/* Wait until ADDR is set: EV6 */
	while ((i2c_x->SR1 & 0x0002) != 0x0002) {
		if (Timeout-- == 0)
			return Error;
	}
	/* Clear ADDR flag by reading SR2 register */
	temp = i2c_x->SR2;
	if (i2c_x == I2C1) {
		/* Wait until DMA end of transfer */
		while (!DMA_GetFlagStatus(DMA1_FLAG_TC7));
		/* Disable DMA Channel */
		DMA_Cmd(I2C1_DMA_CHANNEL_RX, DISABLE);
		/* Clear the DMA Transfer Complete flag */
		DMA_ClearFlag(DMA1_FLAG_TC7);
	} else if (i2c_x == I2C2) {
		/* Wait until DMA end of transfer */
		while (!DMA_GetFlagStatus(DMA1_FLAG_TC5));
		/* Disable DMA Channel */
		DMA_Cmd(I2C2_DMA_CHANNEL_RX, DISABLE);
		/* Clear the DMA Transfer Complete flag */
		DMA_ClearFlag(DMA1_FLAG_TC5);
	}
	/* Program the STOP */
	i2c_x->CR1 |= CR1_STOP_Set;
	/* Make sure that the STOP bit is cleared by Hardware before CR1 write access */
	while ((i2c_x->CR1 & 0x200) == 0x200);
    return Success;
} //master_read()


Status I2C::master_write(const uint8_t* _buffer_p, uint32_t _number_of_bytes, uint8_t _slave_address) {
    __IO uint32_t temp = 0;
    __IO uint32_t Timeout = 0;

    // Enable Error IT
    //i2c_x->CR2 |= I2C_IT_ERR;

	Timeout = 0xFFFF;
	/* Configure the DMA channel for I2Cx transmission */
	i2c_dma_config(_buffer_p, _number_of_bytes, I2C_DIRECTION_TX);
	/* Enable the I2Cx DMA requests */
	i2c_x->CR2 |= CR2_DMAEN_Set;
	/* Send START condition */
	i2c_x->CR1 |= CR1_START_Set;
	/* Wait until SB flag is set: EV5 */
	while ((i2c_x->SR1 & 0x0001) != 0x0001)	{
		if (Timeout-- == 0)
			return Error;
	}
	Timeout = 0xFFFF;
	/* Send slave address */
	/* Reset the address bit0 for write */
	_slave_address &= OAR1_ADD0_Reset;
	i2c_x->DR = _slave_address;
	/* Wait until ADDR is set: EV6 */
	while ((i2c_x->SR1 & 0x0002) != 0x0002) {
		if (Timeout-- == 0)
			return Error;
	}
	/* Clear ADDR flag by reading SR2 register */
	temp = i2c_x->SR2;
	if (i2c_x == I2C1)
	{
		/* Wait until DMA end of transfer */
		while (!DMA_GetFlagStatus(DMA1_FLAG_TC6));
		/* Disable the DMA1 Channel 6 */
		DMA_Cmd(I2C1_DMA_CHANNEL_TX, DISABLE);
		/* Clear the DMA Transfer complete flag */
		DMA_ClearFlag(DMA1_FLAG_TC6);
	} else if (i2c_x == I2C2) {
		/* Wait until DMA end of transfer */
		while (!DMA_GetFlagStatus(DMA1_FLAG_TC4));
		/* Disable the DMA1 Channel 4 */
		DMA_Cmd(I2C2_DMA_CHANNEL_TX, DISABLE);
		/* Clear the DMA Transfer complete flag */
		DMA_ClearFlag(DMA1_FLAG_TC4);
	}
	/* EV8_2: Wait until BTF is set before programming the STOP */
	while ((i2c_x->SR1 & 0x00004) != 0x000004);
	/* Program the STOP */
	i2c_x->CR1 |= CR1_STOP_Set;
	/* Make sure that the STOP bit is cleared by Hardware */
	while ((i2c_x->CR1 & 0x200) == 0x200);
    return Success;
} //master_write()


Status I2C::master_write(uint8_t _first_byte, const uint8_t* _buffer_p, uint32_t _number_of_bytes, uint8_t _slave_address) {
    __IO uint32_t temp = 0;
    __IO uint32_t Timeout = 0;

    // Enable Error IT
    //i2c_x->CR2 |= I2C_IT_ERR;

	Timeout = 0xFFFF;
	/* Configure the DMA channel for I2Cx transmission */
//	i2c_dma_config(_buffer_p, _number_of_bytes, I2C_DIRECTION_TX);
	i2c_dma_config(&_first_byte, 1, I2C_DIRECTION_TX);
	/* Enable the I2Cx DMA requests */
	i2c_x->CR2 |= CR2_DMAEN_Set;
	/* Send START condition */
	i2c_x->CR1 |= CR1_START_Set;
	/* Wait until SB flag is set: EV5 */
	while ((i2c_x->SR1 & 0x0001) != 0x0001)	{
		if (Timeout-- == 0)
			return Error;
	}
	Timeout = 0xFFFF;
	/* Send slave address */
	/* Reset the address bit0 for write */
	_slave_address &= OAR1_ADD0_Reset;
	i2c_x->DR = _slave_address;
	/* Wait until ADDR is set: EV6 */
	while ((i2c_x->SR1 & 0x0002) != 0x0002) {
		if (Timeout-- == 0)
			return Error;
	}

	/* Clear ADDR flag by reading SR2 register */
	temp = i2c_x->SR2;
	if (i2c_x == I2C1)
	{
		/* Wait until DMA end of transfer */
		while (!DMA_GetFlagStatus(DMA1_FLAG_TC6));
		/* Disable the DMA1 Channel 6 */
		DMA_Cmd(I2C1_DMA_CHANNEL_TX, DISABLE);
		/* Clear the DMA Transfer complete flag */
		DMA_ClearFlag(DMA1_FLAG_TC6);
	} else if (i2c_x == I2C2) {
		/* Wait until DMA end of transfer */
		while (!DMA_GetFlagStatus(DMA1_FLAG_TC4));
		/* Disable the DMA1 Channel 4 */
		DMA_Cmd(I2C2_DMA_CHANNEL_TX, DISABLE);
		/* Clear the DMA Transfer complete flag */
		DMA_ClearFlag(DMA1_FLAG_TC4);
	}

	/* Clear ADDR flag by reading SR2 register */
	i2c_dma_config(_buffer_p, _number_of_bytes, I2C_DIRECTION_TX);
	temp = i2c_x->SR2;
	if (i2c_x == I2C1)
	{
		/* Wait until DMA end of transfer */
		while (!DMA_GetFlagStatus(DMA1_FLAG_TC6));
		/* Disable the DMA1 Channel 6 */
		DMA_Cmd(I2C1_DMA_CHANNEL_TX, DISABLE);
		/* Clear the DMA Transfer complete flag */
		DMA_ClearFlag(DMA1_FLAG_TC6);
	} else if (i2c_x == I2C2) {
		/* Wait until DMA end of transfer */
		while (!DMA_GetFlagStatus(DMA1_FLAG_TC4));
		/* Disable the DMA1 Channel 4 */
		DMA_Cmd(I2C2_DMA_CHANNEL_TX, DISABLE);
		/* Clear the DMA Transfer complete flag */
		DMA_ClearFlag(DMA1_FLAG_TC4);
	}


	/* EV8_2: Wait until BTF is set before programming the STOP */
	while ((i2c_x->SR1 & 0x00004) != 0x000004);
	/* Program the STOP */
	i2c_x->CR1 |= CR1_STOP_Set;
	/* Make sure that the STOP bit is cleared by Hardware */
	while ((i2c_x->CR1 & 0x200) == 0x200);
    return Success;
} //master_write()
