#include "Piezo.h"
#include "bluepill.h"
#include "stm32f10x_tim.h"


u16 steps = 0;		//global variable used by TIM2_IRQHandler()



Piezo::Piezo(u8 piezo_pin_param) {
	piezo_pin = Pin(piezo_pin_param, GPIO_Mode_AF_PP);
	duty_cycle = 0;
	frequency = 0;
	time_period_us = 0;
	beep_duration = 0;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
    // if MCU frequency 72 MHz, prescaler of value 72 will make 1us counter steps
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStructure.TIM_Prescaler = 720 - 1; //10us
    TIM_TimeBaseInitStructure.TIM_Period = time_period_us / 10;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);

    TIM_OCInitTypeDef TIM_OCInitStructure;
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;//TIM_OCMode_Toggle;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_OutputNState = TIM_OutputState_Disable;
    TIM_OCInitStructure.TIM_Pulse = (TIM_TimeBaseInitStructure.TIM_Period * duty_cycle) / 100;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;
    TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
    TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
	TIM_OC2Init(TIM2, &TIM_OCInitStructure);
    //TIM_SetCompare2(TIM2, 5000);

    //TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
    //TIM_ITConfig(TIM2, TIM_IT_Update|TIM_IT_CC1, ENABLE);


    TIM_Cmd(TIM2, ENABLE);


    NVIC_InitTypeDef NVIC_InitStruct;
    NVIC_InitStruct.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 8;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStruct);

    TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
    TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);


    /*
    TIM_OCInitTypeDef  TIM_OCInitStructure;
    TIM_OCStructInit (&TIM_OCInitStructure);
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Active;

    TIM_OCInitStructure.TIM_Pulse = 20000;
    TIM_OC1Init (TIM2, &TIM_OCInitStructure);

    TIM_OCInitStructure.TIM_Pulse = 40000;
    TIM_OC2Init (TIM2, &TIM_OCInitStructure);
    */

} //Piezo()



void Piezo::beep(u16 frequency_param, u16 beep_duration_ms_param, u8 pulse_width_param) {

	if (frequency_param > 10000) {
		frequency_param = 10000;
	}

	duty_cycle = pulse_width_param;
	frequency = frequency_param;
	time_period_us = 1000000 / frequency;
	beep_duration = beep_duration_ms_param;

	::steps = beep_duration * frequency / 1000;


    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

    TIM_TimeBaseInitTypeDef timerInitStructure;
    // if MCU frequency 72 MHz, prescaler of value 72 will make 1us counter steps
    timerInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    timerInitStructure.TIM_Prescaler = 720 - 1;
    timerInitStructure.TIM_Period = time_period_us / 10;
    timerInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInit(TIM2, &timerInitStructure);

    TIM_OCInitTypeDef TIM_OCInitStructure;
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;//TIM_OCMode_Toggle;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_OutputNState = TIM_OutputState_Disable;
    TIM_OCInitStructure.TIM_Pulse = (timerInitStructure.TIM_Period * duty_cycle) / 100;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_Low;
    TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
    TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset;
	TIM_OC2Init(TIM2, &TIM_OCInitStructure);

    TIM_Cmd(TIM2, ENABLE);

    TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
    TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);

    //pulse_width = 10000 / frequency_param;
    //TIM_SetCompare1(TIM2, duty_cycle);
    //TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
    //TIM_ITConfig(TIM2, TIM_IT_Update|TIM_IT_CC1, ENABLE);

} //beep()



void TIM2_IRQHandler(void) {

	//if (TIM_GetITStatus(TIM2, TIM_IT_Update) == SET) {
	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	//}

	steps--;

	if (steps == 0) {
		TIM_Cmd(TIM2, DISABLE);
	    TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
	}
/*
	} else if (TIM_GetITStatus(TIM2, TIM_IT_CC1) == SET) {
		TIM_ClearITPendingBit(TIM2, TIM_IT_CC1);
		buzzer.piezo_pin.digital_toggle();
		TIM_SetCompare1(TIM2, TIM_GetCapture1(TIM2) + buzzer.pulse_width);
	}
*/
} //TIM2_IRQHandler()


