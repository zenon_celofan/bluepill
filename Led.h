#ifndef LED_H_
#define LED_H_

#include "stm32f10x_gpio.h"
#include "bluepill.h"
#include "Pin.h"

#define ACTIVE_LOW		Bit_RESET
#define ACTIVE_HIGH		Bit_SET



class Led : public Pin {

private:
	BitAction		active_state;


public:
	Led(uint16_t led_pin = PC13, BitAction define_active_state = Bit_RESET);

	void turn_on(void);
	void turn_off(void);
	void toggle(void);
	void set(uint8_t new_state = 0x01);

	uint8_t digital_read(void) = delete;

};


#endif /* LED_H_ */
