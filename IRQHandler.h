#ifndef IRQHANDLER_H_
#define IRQHANDLER_H_

extern "C" void TIM2_IRQHandler(void);
extern "C" void TIM3_IRQHandler(void);
extern "C" void TIM4_IRQHandler(void);

extern "C" void EXTI0_IRQHandler(void);
extern "C" void EXTI1_IRQHandler(void);
extern "C" void EXTI2_IRQHandler(void);
extern "C" void EXTI3_IRQHandler(void);
extern "C" void EXTI4_IRQHandler(void);
extern "C" void EXTI9_5_IRQHandler(void);
extern "C" void EXTI15_10_IRQHandler(void);

extern "C" void PVD_IRQHandler(void);
extern "C" void RTC_IRQHandler(void);
extern "C" void TAMPER_IRQHandler(void);
extern "C" void RTCAlarm_IRQHandler(void);
extern "C" void USBWakeUp_IRQHandler(void);

extern "C" void USART1_IRQHandler(void);
extern "C" void USART2_IRQHandler(void);
extern "C" void USART3_IRQHandler(void);

extern "C" void USB_LP_CAN1_RX0_IRQHandler(void);

#endif /* IRQHANDLER_H_ */
