#ifndef PIEZO_H_
#define PIEZO_H_

#include "bluepill.h"
#include "Pin.h"

//extern "C" {
//	void TIM2_IRQHandler(void);
//}



class Piezo {

private:
	Pin piezo_pin;
	u8 duty_cycle;
	u16 beep_duration;
	u16 frequency;
	u16 time_period_us;


public:
	Piezo(u8 piezo_pin_param);

	void beep(u16 frequency_param, u16 beep_duration_ms_param, u8 duty_cycle = 50) ;

friend void TIM2_IRQHandler(void);

};




#endif /* PIEZO_H_ */
