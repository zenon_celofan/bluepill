#include "Led.h"
#include "stm32f10x_gpio.h"
#include "Pin.h"



Led::Led(uint16_t led_pin, BitAction define_active_state) : Pin(led_pin, GPIO_Mode_Out_OD, GPIO_Speed_2MHz, (BitAction) !define_active_state) {

	active_state = define_active_state;

} //Led()



void Led::turn_on(void) {

	Pin::digital_write(active_state);

} //turn_on()



void Led::turn_off(void) {

	Pin::digital_write(!active_state);

} //turn_off()



void Led::toggle(void) {

	Pin::digital_toggle();

} //toggle()



void Led::set(uint8_t new_state) {

	Pin::digital_write((BitAction) new_state);

} //set()
