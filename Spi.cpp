#include "Spi.h"
#include "Pin.h"


Spi::Spi(SPI_TypeDef* spi_x_param, bool remap_param, SPI_InitTypeDef* SPI_InitStructure_param)
: spi(spi_x_param) {

	init_pins(remap_param);

	SPI_I2S_DeInit(spi);

	if (SPI_InitStructure_param == NULL) {
		SPI_InitTypeDef  SPI_InitStructure;
		SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
		SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
		SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
		SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
		SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
		SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
		SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;   //default = 8
		SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
		SPI_InitStructure.SPI_CRCPolynomial = 7;
		SPI_CalculateCRC(spi, DISABLE);

		init_spi(&SPI_InitStructure);
	} else {
		init_spi(SPI_InitStructure_param);
	}

} //Spi()



void Spi::init_spi(SPI_InitTypeDef* SPI_InitStructure_param) {

	if (spi == SPI1) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
	} else if (spi == SPI2) {
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
	}

	SPI_Init(spi, SPI_InitStructure_param);
	SPI_Cmd(spi, ENABLE);

} //init_spi()



void Spi::init_pins(bool remap_param) {

	if (spi == SPI1) {
		if (remap_param == true) {
			GPIO_PinRemapConfig(GPIO_Remap_SPI1, ENABLE);
			miso_pin = Pin(PB4, GPIO_Mode_IN_FLOATING);
			mosi_pin = Pin(PB5, GPIO_Mode_AF_PP, GPIO_Speed_50MHz);
			sck_pin = Pin(PB3, GPIO_Mode_AF_PP, GPIO_Speed_50MHz);
		} else {
			GPIO_PinRemapConfig(GPIO_Remap_SPI1, DISABLE);
			miso_pin = Pin(PA6, GPIO_Mode_IN_FLOATING);
			mosi_pin = Pin(PA7, GPIO_Mode_AF_PP, GPIO_Speed_50MHz);
			sck_pin = Pin(PA5, GPIO_Mode_AF_PP, GPIO_Speed_50MHz);
		}
	} else if (spi == SPI2) {
		miso_pin = Pin(PB14, GPIO_Mode_IN_FLOATING);
		mosi_pin = Pin(PB15, GPIO_Mode_AF_PP, GPIO_Speed_50MHz);
		sck_pin = Pin(PB13, GPIO_Mode_AF_PP, GPIO_Speed_50MHz);
	}

} //init_pins()



void Spi::write_byte(u8 byte) {

	/* Loop while DR register in not empty */
	while(SPI_I2S_GetFlagStatus(spi, SPI_I2S_FLAG_TXE) == RESET);

	/* Send byte through the SPI2 peripheral */
	spi->DR = byte;

	while(SPI_I2S_GetFlagStatus(spi, SPI_I2S_FLAG_TXE) == RESET);
	/* Wait to receive a byte */
	//while(SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_RXNE) == RESET);

	/* Return the byte read from the SPI bus */
	//return SPI_I2S_ReceiveData(SPI2);
	//spi_x->DR;

} //write_byte()



uint8_t Spi::read_byte(u8 byte) {

	// Clear data register by reading it
	spi->DR;
	while(SPI_I2S_GetFlagStatus(spi, SPI_I2S_FLAG_TXE) == RESET);

	// Send out dummy byte (for clock generation)
	spi->DR = byte;
	while(SPI_I2S_GetFlagStatus(spi, SPI_I2S_FLAG_RXNE) == RESET);

	// Read and return wanted data
	return (u8) spi->DR;

} //read_byte()



bool Spi::is_busy(void) {

	if (SPI_I2S_GetFlagStatus(spi, SPI_I2S_FLAG_BSY) == SET) {
		return true;
	} else {
		return false;
	}

} //is_busy()
